from django.contrib import auth
from django.shortcuts import render, redirect
from django.views.generic import View

from .forms import LoginForm
from .models import UserProfile, Investor, Venture, Admin


class LoginView(View):
    next = None
    template_name = "cfapp/login.html"

    def get(self, request):
        self.next = request.GET.get('next', "")
        if request.user.is_authenticated():
            if request.user.user_type == 'admin':
                return None
            elif request.user.user_type == 'investor':
                return redirect('investor-home')
            elif request.user.user_type == 'venture':
                return redirect('venture-home')
        args = dict(form=LoginForm(None), next=self.next)
        return render(request, self.template_name, args)

    def post(self, request):
        form = LoginForm(request.POST)
        # https://docs.djangoproject.com/en/1.9/topics/http/sessions/#setting-test-cookies
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            remember_me = form.cleaned_data['remember_me']
            if not remember_me:
                request.session.set_expiry(0)
            try:
                user_profile = UserProfile.objects.get(username=username)
            except UserProfile.DoesNotExist:
                user_profile = None
            if user_profile is not None:
                user_type = user_profile.user_type
                user = auth.authenticate(username=username, password=password)
                if user is not None:
                    if user_type == "admin":
                        auth.login(request, user)
                        return redirect('admin-home')
                    elif user_type == "investor":
                        auth.login(request, user)
                        return redirect('investor-home')
                    elif user_type == "venture":
                        auth.login(request, user)
                        return redirect('venture-home')
                    else:
                        form.add_error(None, 'Incorrect password.')
                        return render(request, self.template_name, dict(form=form))
                else:
                    form.add_error(None, 'User not active yet.')
                    return render(request, self.template_name, dict(form=form))
            else:
                form.add_error(None, 'Invalid username')
                return render(request, self.template_name, dict(form=form))
        else:
            return render(request, self.template_name, dict(form=form))


class LogoutView(View):
    http_method_names = ['get', 'head', 'options']

    def get(self, request):
        auth.logout(request=request)
        return redirect('login')
