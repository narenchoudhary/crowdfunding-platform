from django.shortcuts import get_object_or_404, render
from django.views.generic import View

from .models import Admin


class Home(View):
    template_name = 'cfapp/admin/home.html'

    def get(self, request):
        admin = get_object_or_404(
            Admin, user_profile__username=request.user.username)
        return render(request, self.template_name, dict(admin=admin))
