# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-29 13:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cfapp', '0013_ventureequitycampaign_closed'),
    ]

    operations = [
        migrations.AddField(
            model_name='venture',
            name='location',
            field=models.CharField(default='Mumbai, Inida', max_length=20),
        ),
    ]
