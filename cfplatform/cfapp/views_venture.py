import os
import uuid


from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic import View, TemplateView, CreateView, DetailView, ListView

from .models import Venture, Key, VentureEquityCampaign, UserProfile, VentureDocument
from .forms import CreateVentureEquityCampaignForm, VentureSignup
from .utils import generate_sign, send_file


class SignUp(View):
    template_name = "cfapp/venture/signup.html"
    signup_confirm = "cfapp/venture/signupconfirm.html"

    def get(self, request):
        return render(request, self.template_name, dict(form=VentureSignup(None)))

    def post(self, request):
        form = VentureSignup(request.POST, request.FILES)
        if form.is_valid():
            username = form.cleaned_data['username']
            venture = form.save(commit=False)
            user_profile = UserProfile(username=username, user_type='venture')
            user_profile.save()
            venture.user_profile = user_profile
            venture.save()
            return render(request, self.signup_confirm, dict(venture=venture))
        else:
            return render(request, self.template_name, dict(form=form))


class Home(View):
    template_name = 'cfapp/venture/home.html'

    def get(self, request):
        venture = get_object_or_404(
            Venture, user_profile__username=request.user.username)
        try:
            key = Key.objects.get(venture=venture)
        except Key.DoesNotExist:
            key = None
        context = dict(venture=venture, key=key)
        return render(request, self.template_name, context)


class CreateKey(View):
    http_method_names = ['get', 'head', 'options']

    def get(self, request):
        user_id = request.user.id
        key = Key()
        key.venture = Venture.objects.get(user_profile__id=user_id)
        key.public_key = uuid.uuid4().hex
        key.private_key = uuid.uuid4().hex
        key.save()
        generate_sign(str(user_id))
        return redirect('venture-get-key')


class GetKey(TemplateView):
    template_name = 'cfapp/venture/key.html'

    def get_context_data(self, **kwargs):
        context = super(GetKey, self).get_context_data(**kwargs)
        context['key'] = Key.objects.get(venture__user_profile__id=self.request.user.id)
        return context


class CreateVentureEquityCampaign(CreateView):
    form_class = CreateVentureEquityCampaignForm
    template_name = 'cfapp/venture/create_vec.html'

    def form_valid(self, form):
        vec = form.save(commit=False)
        vec.venture = Venture.objects.get(user_profile__id=self.request.user.id)
        vec.save()
        return redirect('venture-vec-detail', pk=vec.id)


class VentureEquityCampaignDetail(DetailView):
    model = VentureEquityCampaign
    template_name = 'cfapp/venture/vec_detail.html'
    context_object_name = 'vec'


class VentureEquityCampaignList(ListView):
    model = VentureEquityCampaign
    template_name = 'cfapp/venture/vec_list.html'
    context_object_name = 'vec_list'


class CreateDocument(CreateView):
    model = VentureDocument
    fields = ['name', 'document']
    template_name = 'cfapp/venture/create_document.html'

    def form_valid(self, form):
        doc = form.save(commit=False)
        doc.venture = Venture.objects.get(user_profile__id=self.request.user.id)
        doc.save()
        filepath = doc.document.path
        print(filepath)
        send_file(filepath, str(self.request.user.id))
        return redirect('venture-doc-list')


class DocumentDetail(DetailView):
    model = VentureDocument
    template_name = 'cfapp/venture/document_detail.html'
    context_object_name = 'doc'


class DocumentList(ListView):
    model = VentureDocument
    template_name = 'cfapp/venture/document_list.html'
    context_object_name = 'doc_list'

    def get_queryset(self):
        return VentureDocument.objects.filter(
            venture__user_profile__id=self.request.user.id
        )


class DownloadDoc(View):

    def get(self, request, pk):
        doc = VentureDocument.objects.get(id=pk)
        file = doc.document
        response = HttpResponse(file, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=%s' % doc.name
        return response
