from django.conf.urls import url

from . import views, views_admin, views_investor, views_venture

urlpatterns = [
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^admin/home/$', views_admin.Home.as_view(), name='admin-home'),

    url(r'^investor/home/$', views_investor.Home.as_view(), name='investor-home'),
    url(r'^investor/list/ventures/$', views_investor.VentureEquityCampaignList.as_view(),
        name='investor-list-ventures'),
    url(r'^investor/get/venture/(?P<pk>\d+)/$', views_investor.VentureEquityCampaignDetail.as_view(),
        name='investor-get-venture'),
    url(r'^investor/venture/(?P<pk>\d+)/create/fund/$', views_investor.CreateInvestorFund.as_view(),
        name='investor-create-fund'),

    url(r'^venture/signup/$', views_venture.SignUp.as_view(), name='venture-signup'),
    url(r'^venture/home/$', views_venture.Home.as_view(), name='venture-home'),
    url(r'^venture/get/key/$', views_venture.GetKey.as_view(), name='venture-get-key'),
    url(r'^venture/create/key/$', views_venture.CreateKey.as_view(), name='venture-create-key'),
    url(r'^venture/create/vec/$', views_venture.CreateVentureEquityCampaign.as_view(),
        name='venture-vec-create'),
    url(r'^venture/get/vec/(?P<pk>\d+)/$', views_venture.VentureEquityCampaignDetail.as_view(),
        name='venture-vec-detail'),
    url(r'^venture/list/vec/$', views_venture.VentureEquityCampaignList.as_view(),
        name='venture-vec-list'),

    url(r'^venture/create/doc/$', views_venture.CreateDocument.as_view(),
        name='venture-doc-create'),
    url(r'^venture/get/doc/(?P<pk>\d+)/$', views_venture.DocumentDetail.as_view(),
        name='venture-doc-detail'),
    url(r'^venture/list/doc/$', views_venture.DocumentList.as_view(),
        name='venture-doc-list'),

    url(r'^venture/get/download/(?P<pk>\d+)/$', views_venture.DownloadDoc.as_view(),
        name='venture-doc-download'),
]
