from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404, render
from django.views.generic import View, ListView, DetailView, CreateView

from .models import Investor, Venture, InvestorFund, VentureEquityCampaign


class Home(View):
    template_name = 'cfapp/investor/home.html'

    def get(self, request):
        investor = get_object_or_404(
            Investor, user_profile__username=request.user.username)
        return render(request, self.template_name, dict(investor=investor))


class VentureEquityCampaignList(ListView):
    model = VentureEquityCampaign
    paginate_by = 10
    template_name = 'cfapp/investor/venture_list.html'
    context_object_name = 'vec_list'


class VentureEquityCampaignDetail(DetailView):
    model = VentureEquityCampaign
    template_name = 'cfapp/investor/venture_detail.html'
    context_object_name = 'vec'

    def get_context_data(self, **kwargs):
        context = super(VentureEquityCampaignDetail, self).get_context_data(**kwargs)
        try:
            context['fund'] = InvestorFund.objects.get(vec__id=self.kwargs['pk'])
        except InvestorFund.DoesNotExist:
            context['fund'] = None
        return context


class CreateInvestorFund(CreateView):
    model = InvestorFund
    fields = ['amount_invested']
    template_name = 'cfapp/investor/create_fund.html'
    success_template = "cfapp/investor/venture_detail.html"

    def get_context_data(self, **kwargs):
        context = super(CreateInvestorFund, self).get_context_data(**kwargs)
        context['vec'] = VentureEquityCampaign.objects.get(id=self.kwargs['pk'])
        return context

    def get_form(self, form_class=None):
        form = super(CreateInvestorFund, self).get_form(form_class)
        form.fields['amount_invested'].label = "Amount"
        return form

    def form_valid(self, form):
        fund = form.save(commit=False)
        vec = VentureEquityCampaign.objects.get(id=self.kwargs['pk'])
        vec.raised_so_far += fund.amount_invested
        vec.save()
        fund.vec = vec
        fund.save()
        context = dict(fund=fund, vec=vec, new=True)
        return render(self.request, self.success_template, context)


class InvestorFundList(ListView):
    model = InvestorFund
    template_name = 'cfapp/investor/fund_list.html'
    success_url = reverse_lazy('investor-fund-list')
