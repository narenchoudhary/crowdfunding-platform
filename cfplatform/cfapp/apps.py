from django.apps import AppConfig


class CfappConfig(AppConfig):
    name = 'cfapp'
