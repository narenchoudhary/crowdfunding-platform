import os
import hashlib
from Crypto.PublicKey import RSA
import nacl.encoding
import nacl.signing
import pickle

privateSign = "privateSign"
publicSign = "publicSign"
doc_dest = 'ledger/docs'
file_index = 'ledger/file_index.p'
tmp_file = 'tmp'


def generate_sign(user):
    script_dir = os.path.dirname(__file__)
    signing_key = nacl.signing.SigningKey.generate()
    rel_path = privateSign+"/"+user+".p"
    abs_file_path = os.path.join(script_dir, rel_path)
    pickle.dump(signing_key, open(abs_file_path, "wb"))

    verify_key = signing_key.verify_key
    verify_key_hex = verify_key.encode(encoder=nacl.encoding.HexEncoder)
    rel_path = publicSign + "/" + user + ".p"
    abs_file_path = os.path.join(script_dir, rel_path)
    pickle.dump(verify_key_hex, open(abs_file_path, "wb"))


def sign_with_private(file, user):
    script_dir = os.path.dirname(__file__)
    rel_path = privateSign + "/" + user + ".p"
    abs_file_path = os.path.join(script_dir, rel_path)
    with open(abs_file_path, 'rb') as handle:
        signing_key = pickle.load(handle)
    in_file = open(file, "rb")  # opening for [r]eading as [b]inary
    data = in_file.read()  # if you only wanted to read 512 bytes, do .read(512)
    in_file.close()
    signed = signing_key.sign(data)
    return signed


def get_sha256(filename):
    sha256 = hashlib.sha256(filename)
    return sha256.hexdigest()


def export_keys():
    return


def send_file(filename, user):
    signed = sign_with_private(filename, user)
    # miners = [IP1,...]
    # send_signed_to_miners
    return


def get_file_index():
    try:
        with open( file_index, 'rb') as handle:
            index_file = pickle.load(handle)
        return index_file
    except IOError:
        l = list()
        return l


def store_file_index(index_file):
    pickle.dump( index_file, open( file_index, "wb" ) )


def store(message, hash1 ,user,doc_name):
    index_file = get_file_index()

    if len(index_file)!=0:
        last_line = index_file[-1]['hash']
        hash2 =  last_line + hash1
        hash1 = get_sha256(hash2.encode('utf-8'))

    index_file.append( {'user':user,'doc_name':doc_name,'hash':hash1} )
    store_file_index(index_file)
    pickle.dump( message, open( doc_dest+"/"+doc_name+".p", "wb" ) )


def insert(signed_doc,user,doc_name):
    if check_sign(user,signed_doc):
        store(signed_doc.message,get_sha256(signed_doc.message),user,doc_name)
    else:
        print("Invalid signature!!")


def check_sign(user,signed):
    with open( publicSign+"/"+user+".p", 'rb') as handle:
        verify_key_hex = pickle.load(handle)
    verify_key = nacl.signing.VerifyKey(verify_key_hex, encoder=nacl.encoding.HexEncoder)
    try:
        verify_key.verify(signed)
        return 1
    except:
        return 0


def verify():
    index_file = get_file_index()
    print(index_file)
    if(len(index_file)==0):
        return 1
    file_name_local = index_file[0]['doc_name']

    with open( doc_dest+"/"+file_name_local+".p", 'rb') as handle:
        message = pickle.load(handle)
    hash1 = get_sha256(message)

    if(hash1!=index_file[0]['hash']):
        print("Error in %s block!"%file_name_local)
        return 0
    try:
        for doc in index_file[1:]:
            print("*")
            file_name_local = doc['doc_name']
            with open( doc_dest+"/"+file_name_local+".p", 'rb') as handle:
                message = pickle.load(handle)
            print(message)
            hash1 = get_sha256( (hash1+get_sha256(message)).encode('utf-8') )
            print(hash1)
            print(doc['hash'])
            if(hash1!=doc['hash']):
                print("Error in %s block!"%file_name_local)
                return 0

    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print (message)

    return 1