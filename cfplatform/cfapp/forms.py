from django import forms

from .models import VentureEquityCampaign, Venture


class LoginForm(forms.Form):
    username = forms.CharField(required=True, label='Username', max_length=50)
    password = forms.CharField(required=True, widget=forms.PasswordInput)
    remember_me = forms.BooleanField(
        required=False, initial=False, label='Remember Me',
        widget=forms.CheckboxInput(attrs={'class': 'filled-in'})
    )


class CreateVentureEquityCampaignForm(forms.ModelForm):

    class Meta:
        model = VentureEquityCampaign
        fields = ['equity_percentage', 'currency', 'amount', 'max_equity_per_person', 'deadline']
        widgets = {
            'deadline': forms.DateInput(attrs={'class': 'datepicker'})
        }


class VentureSignup(forms.ModelForm):

    class Meta:
        model = Venture
        fields = ['username', 'email', 'contact_no', 'registered_company_name', 'registered_id_no',
                  'valuation', 'currency', 'capitalization_document', 'description', 'cash_flow_document',
                  'term_sheet_document', 'valuation_document', 'industry_sector', 'logo', 'organization_type']
        widgets = {
            'description': forms.Textarea(attrs={
                'class': 'materialize-textarea'
            })
        }