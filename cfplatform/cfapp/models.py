from __future__ import unicode_literals

import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.html import format_html


from .constants import USER_TYPE


def get_logo_name(instance, filename):
    url = "logo/{0}_{1}.jpg".format(instance.id, uuid.uuid4())
    return url


@python_2_unicode_compatible
class UserProfile(AbstractUser):
    user_type = models.CharField(max_length=20, choices=USER_TYPE)

    def __str__(self):
        return self.username


@python_2_unicode_compatible
class Investor(models.Model):
    user_profile = models.OneToOneField(UserProfile, null=False, blank=False)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.user_profile.username


@python_2_unicode_compatible
class Venture(models.Model):
    user_profile = models.OneToOneField(UserProfile, null=False, blank=False)
    username = models.CharField(max_length=60, null=True, default="hello")
    email = models.EmailField(max_length=100, null=True, default="email@hello.com")
    contact_no = models.CharField(max_length=100, default="801103388")
    industry_sector = models.CharField(max_length=50, default="COMMERCE")
    organization_type = models.CharField(max_length=50, default="SME")
    registered_company_name = models.CharField(max_length=200, default="HELLO INC")
    registered_id_no = models.CharField(max_length=200, default="HELLO")
    description = models.TextField(max_length=500, default="")
    logo = models.ImageField(upload_to=get_logo_name, null=True, blank=True)
    currency = models.CharField(max_length=9, default='USD')
    valuation = models.PositiveIntegerField(default=10000)
    valuation_document = models.FileField(null=True, blank=True)
    capitalization_document = models.FileField(null=True, blank=True)
    term_sheet_document = models.FileField(null=True, blank=True)
    cash_flow_document = models.FileField(null=True, blank=True)
    location = models.CharField(max_length=20, default="Mumbai, Inida")

    def __str__(self):
        return self.user_profile.username

    def image_tag(self):
        """
        By default, admin panel only shows the path of the image file.
        This function is used to show image preview in admin panel.
        Check admin.py for usage.
        :return: HTML Snippet
        :rtype: str
        """
        return format_html(
            '<img src="%s" width="200" height="200" />' % self.logo.url)


@python_2_unicode_compatible
class Admin(models.Model):
    user_profile = models.OneToOneField(UserProfile, null=False, blank=False)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.user_profile.username


@python_2_unicode_compatible
class Key(models.Model):
    venture = models.OneToOneField(Venture, null=False, blank=False)
    public_key = models.CharField(max_length=500)
    private_key = models.CharField(max_length=500)

    def __str__(self):
        return self.venture.user_profile.username


@python_2_unicode_compatible
class VentureEquityCampaign(models.Model):
    venture = models.ForeignKey(Venture, null=False, blank=False)
    equity_percentage = models.DecimalField(default=20, max_digits=4, decimal_places=2)
    currency = models.CharField(default='USD', max_length=30)
    amount = models.PositiveIntegerField(default=20000)
    max_equity_per_person = models.DecimalField(default=1, max_digits=4, decimal_places=2)
    creation_datetime = models.DateTimeField(null=True, blank=True)
    last_update_datetime = models.DateTimeField(null=True, blank=True)
    deadline = models.DateField(null=True, blank=True)
    closed = models.BooleanField(default=True)
    price_per_share = models.PositiveIntegerField(default=1)
    num_shares = models.PositiveIntegerField(default=1000)
    max_shares_per_person = models.PositiveIntegerField(default=100)
    max_invest_per_person = models.PositiveIntegerField(default=100)
    total_amount = models.PositiveIntegerField(default=10000)
    raised_so_far = models.PositiveIntegerField(default=0)
    percentage_raised = models.DecimalField(decimal_places=2, max_digits=5, default=0.00)

    def save(self, **kwargs):
        if not self.id:
            self.creation_datetime = timezone.now()
        self.last_update_datetime = timezone.now()
        self.total_amount = self.num_shares * self.price_per_share
        self.percentage_raised = (self.raised_so_far / self.total_amount) * 100
        self.max_invest_per_person = self.max_shares_per_person * self.price_per_share
        super(VentureEquityCampaign, self).save(**kwargs)

    def __str__(self):
        return self.venture.registered_company_name + ' selling ' + str(self.num_shares)  + ' shares'


@python_2_unicode_compatible
class VentureLoanCampaign(models.Model):
    venture = models.ForeignKey(Venture, null=False, blank=False)
    amount = models.PositiveIntegerField(default=20000)
    currency = models.CharField(default='USD', max_length=30)
    duration = models.PositiveIntegerField(default=20)


@python_2_unicode_compatible
class VenturePreferredLoanCampaign(models.Model):
    venture = models.OneToOneField(Venture, null=False, blank=False)
    amount = models.PositiveIntegerField(default=20000)
    currency = models.CharField(default='USD', max_length=30)
    preferred_ros = models.PositiveIntegerField(default=8)
    loan_interest = models.PositiveIntegerField(default=8)


@python_2_unicode_compatible
class InvestorFund(models.Model):
    vec = models.OneToOneField(VentureEquityCampaign)
    amount_invested = models.PositiveIntegerField()


@python_2_unicode_compatible
class VentureDocument(models.Model):
    venture = models.ForeignKey(Venture)
    name = models.CharField(max_length=60)
    document = models.FileField()
    hash = models.CharField(max_length=200)
    verifier = models.CharField(max_length=200)
    creation_datetime = models.DateTimeField()

    def __str__(self):
        return self.venture.registered_company_name + ' ' + self.name

    def save(self, **kwargs):
        if not self.id:
            self.creation_datetime = timezone.now()
        self.hash = uuid.uuid4().hex
        super(VentureDocument, self).save(**kwargs)
